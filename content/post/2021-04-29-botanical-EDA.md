---
title: Machine learning or botanical learning
subtitle: Why do humans NEED to have everything DEAD in order to understand it?
date: 2021-04-29
tags: ["quantum", "botanical"]
---

Exactly [where ML might play a role](https://semiengineering.com/roadblocks-for-ml-in-eda/) in EDA is yet to be decided ... but plants do complex signaling and control in manners that appear to be so deceptively simple that humans, ie the species of needless complexicator and weedkiller-addicts, cannot BEGIN to understand how plants work ... of course, plants do not work ... like a stupid machine that must be programmed to be intelligent ... plants LIVE in intelligent living communities comprised of intelligent living organisms.  This is something that humans suck at ... but once they wake up to it, they believe they invented it.

So ... when it comes EDA, perhaps humans should allow plants to sit in the designer's Aeron seat and drive the design organically.  The real question is WHY do we put computers in boxes or cute little packages ... why don't we use something like Baubotanik human-sculpted-but-tree-grown LIVING architecture .
