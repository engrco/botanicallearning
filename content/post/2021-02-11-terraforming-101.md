---
title: Terraforming 101
subtitle: Introduction to Biosystems MacroEngineering
date: 2021-02-11
tags: ["carbon", "sequestration", "soil"]
---

## Creation 

The first assignment is to read and be completely familiar with the entire story of Creation -- read it backward and forward as a sophisticated allegory that was designed to make you [and all of your ancestors] THINK deeply about the intention of Creation.

WHY is our profoundly stupid, clueless, partially-abled but ridiculously arrogant and conceited pathetic excuse for a species here?  Are we here to appreciate Creation? Are we here to understand Creation? Are we here to learn about how to Create? Are we here to learn about what we can and cannot do, ie are we here to learn humility?

This is why the first assignment is to study EVERY last thing that you can find on humankind's thoughts on Creation ... you are going to need ALL of it.

## Soil Building and Mycorrhizal Networks

The next assignment is to understand soil ecoystems, soil quality, soil laboratories and especially the botanoneurosystem of [mycorrhizal networks](https://en.wikipedia.org/wiki/Mycorrhizal_network) better than all humans beings and all soil scientists have understood soils before. Carbon sequestration is fundamentally about soil building and how we go about converting the luxurious abundance of atmospheric carbon into the soil organic matter which is necessary to produce high-yielding perenniel crops that furnish us with biochemical compounds and polymers that are orders of magnitude more valuable than anything produced now in agriculture.

When you're finished with the Creation and Soil Building assignments, we will have the basis to start learning about how we can engineer terraforming operations.