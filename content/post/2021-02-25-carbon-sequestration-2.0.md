---
title: Carbon Sequestration 2.0
subtitle: Carbon nanotubes, pyrolysis, biochar, soil building, perennial polyculture 
date: 2021-01-01
tags: ["carbon", "sequestration"]
---

As we've said before, carbon is VERY old news...we should understand what works from having our hands dirty -- and practical awareness of Nature and gardening, should understand why the best way to sequester carbon is to put it to better use cycle of Life and attempt to have some understanding of where we might be going ... carbon sequentration matters because of what it might teach us about terraforming or using a larger form of agriculture to make more of the Universe habitable for our species.

You might have different frameworks of thought or reference points to ponder in your mental voyage, but just so that we have similar terminology, how about if we use the [Kardashev Scale of thinking about a civilization's advancement based on the amount of energy it is able to channel into productive uses](https://en.wikipedia.org/wiki/Kardashev_scale) ... and for whatever it's worth, I don't have any confidence that our species will ever ready to advance to a level omega or multi-universal civilization, but that's just me, you are more than welcome to prove me wrong -- my problem with the Kardashev Scale is that I see the importance of ENERGY, ie 1st Law of Thermo stuff, but I think that the Kardashev Scale ignores 2nd Law stuff, ie how Life builds order and fights Entropy.  In other words, if we are JUST enamored with energy, we ignore Life and Love and, that means we [mindlessly accelerate the 2nd law effects](https://twitter.com/QualityFellow/status/1363756710360399872).

Carbon Sequestration 2.0 is about LIFE ... and love ... and using the cycle of Life ... and love ... to overcome the rapid increases in entropy that accompany a rapid release of energy. It's not really about energy, although we do need to channel energy to accomplish things ... it's really about Life ... and if we don't understand something about legitimate Love ... not romance or pornography, but legit Love ... we are never going to get to the positions of creating new universes [and we don't deserve to]. The point of dreaming is too look at the horizon and intuitive think about the best direction to head ... the best direction for our use of carbon is to understand how it resist entropy, to build greater order, to sustain Life and set the stage for Love, ie Carbon Sequestration 3.0.

