---
title: Photonic bio-neural networks
subtitle: Botantical processing
date: 2021-03-07
tags: ["quantum", "botanical"]
---

Sure, you could use a [custom-designed large-scale optical processor](https://twitter.com/peterlmcmahon/status/1387569516155125761) ... but plants already have this figure out -- *unfortunately, humans don't have plants figured out ... AT ALL.*
