---
title: How do plants communicate? What kinds of signalling happens in soil
subtitle: Is soil life all electrochemical or acoustochemical or, more likely, an asynch multi-transducer affair?  
date: 2021-01-08
tags: ["soil", "signalling"]
---

Never mind the "actuators," how do the "sensors" of soil ecoystems work?

Is soil life [accoustochemical](https://www.sciencedirect.com/science/article/pii/S1350417717303061)?

If we want to get some sense of the scope of this we should look at how a simplistic species uses RF or what kinds of really basic knowledge is necessary to even get something like HAM radio license [which, itself is beyond what maybe 99% or more of the population can comprehend].