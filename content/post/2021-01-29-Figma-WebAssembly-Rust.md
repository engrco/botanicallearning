---
title: Figma, WebAssembly, Rust and the search for data schematics
subtitle: When there's way too much information or noise in the illustration
date: 2021-01-29
tags: ["carbon", "BRISC"]
---

This is primarily about executable specifications ... and the search for hardware specification langauages like [Accellera](https://en.wikipedia.org/wiki/Accellera), [SpecC](https://en.wikipedia.org/wiki/SpecC), [SystemVerilog](https://en.wikipedia.org/wiki/SystemVerilog), [SystemRDL](https://en.wikipedia.org/wiki/SystemRDL), [Chisel](https://en.wikipedia.org/wiki/Chisel_(programming_language)) and similar other [awesome hardware description languages](https://github.com/drom/awesome-hdl) which are used with EDA tools to directly implement a design which has been the EXACT same configuration which has been through a thorough visual illustration/design reviews by enginering teams.  

As with micro- or nanoelectronics, the beauty of this approach, of course, is that there is NO possibility for human error or misunderstanding ... the exact configuration of the reviewed design would ultimately be 3D-printed into the framework of a botanoneurological synapse [as with transistors lithographically printed in the realm of microprocessors, ASICs, FPGAs, etc] ... since we are kind Rust-predisposed, maybe we should start with a serious look at [hoodlum](https://github.com/tcr/hoodlum) or [kaze](https://github.com/yupferris/kaze) ... although these are definitely some of the most [*alfiest of alphas*](https://www.youtube.com/watch?v=YL06i9aiM4o) ... they are worth at least a very quick look, because ultimately ... this thing is probably going to need to migrate in the direction of Rust-ish languages, and web-based design tools based on WebAssembly or implementations of Figma.  Of course, that intuitive attempt to look at where we're heading is even more extreme on the [*alfiest of alphas*](https://www.youtube.com/watch?v=YL06i9aiM4o) ... 


If you're singing along, you might get to the line *... is it wise to be cruel ...*and think, okay ... enough of this happy horseshit ... 