---
title: Pre-BRISC 0.0
subtitle: 40 TRIZ Intuitive Principles Of Botanical.Computer
date: 2021-01-22
tags: ["carbon", "BRISC"]
---

In this post, we introduce thinking behind Botanical.Computer or Botantical.Cloud ... the aim of concretely thinking about 40 intuitions is to begin developing something for an IoT-based distributed computing network, perhaps initially coordinated by central processing units using open source RISC architectures ... but ultimately, the vision is for a network that is truly organic, even self-replicating, a botanical information canopy "running" on botanical computers using a Biological Reduced Instruction Set Architecture or BRISC [to make it possible for humans to understand what plants are trying to tell them].

BRISC will be a distilled or deliberated reduced instruction set that will require a lot of background preparation BEFORE the Big Bang of Rev 0.0

We can expect an incredible amount of revision, collaboration and evolutionary turmoil after 0.0 as well, but pre-BRISC is even more nebulous but the start of the start of the start will kick off with thinking about the 40 intuitive principles of what something sci-fi, like botantical computing, is about.  At this early point, all that we can say with certainty is there is NOT going to be an exact one-for-one pairing between the intuitions and instructions in the set, ie an instruction set might address several inuitions and contradictions simultaneously ... it's impossible to say exactly what it might look like, but ...

It's just that in order to kick off the conversation leading to the instruction set we have to start the conversation somewhere -- as all conversations go, we should expect that it will end somewhere very different ... but we need a start. So, in order to start off this conversation, will we will start off by blatantly plagiarizing, copying or just plain stealing the [set of 40 principles of innovation from the TRIZ algorithm of inventive problem solving](https://triz-journal.com/40-inventive-principles-examples/) ... it is extremely important to emphasize that this list is ONLY a starting point, there will be revisions, edits, additions, consolidations, changes.

# 40 Intuitions

## Segmentation

### Divide an object into independent parts.
* forest, tree, branch, twig, leaf, cell ...
* seed coat, cotyledon, root, shoot ...

### Use a work breakdown structure for a large project.
Make an object easy to disassemble.
Modular furniture
Quick disconnect joints in plumbing

## Increase the degree of fragmentation or segmentation.
Replace solid shades with Venetian blinds.
Use powdered welding metal instead of foil or rod to get better penetration of the joint.
 

## Taking out

Separate an interfering part or property from an object, or single out the only necessary part (or property) of an object.
Locate a noisy compressor outside the building where compressed air is used.
Use fiber optics or a light pipe to separate the hot light source from the location where light is needed.
Use the sound of a barking dog, without the dog, as a burglar alarm.
 

## Local quality

Change an object’s structure from uniform to non-uniform, change an external environment (or external influence) from uniform to non-uniform.
Use a temperature, density, or pressure gradient instead of constant temperature, density or pressure.
Make each part of an object function in conditions most suitable for its operation.
Lunch box with special compartments for hot and cold solid foods and for liquids
Make each part of an object fulfill a different and useful function.
Pencil with eraser
Hammer with nail puller
Multi-function tool that scales fish, acts as a pliers, a wire stripper, a flat-blade screwdriver, a Phillips screwdriver, manicure set, etc.
 

## Asymmetry

Change the shape of an object from symmetrical to asymmetrical.
Asymmetrical mixing vessels or asymmetrical vanes in symmetrical vessels improve mixing (cement trucks, cake mixers, blenders).
Put a flat spot on a cylindrical shaft to attach a knob securely.
If an object is asymmetrical, increase its degree of asymmetry.
Change from circular O-rings to oval cross-section to specialized shapes to improve sealing.
Use astigmatic optics to merge colors.
 

## Merging

Bring closer together (or merge) identical or similar objects, assemble identical or similar parts to perform parallel operations.
Personal computers in a network
Thousands of microprocessors in a parallel processor computer
Vanes in a ventilation system
Electronic chips mounted on both sides of a circuit board or subassembly
Make operations contiguous or parallel; bring them together in time.
Link slats together in Venetian or vertical blinds.
Medical diagnostic instruments that analyze multiple blood parameters simultaneously
Mulching lawnmower
 

## Universality

Make a part or object perform multiple functions; eliminate the need for other parts.
Handle of a toothbrush contains toothpaste
Child’s car safety seat converts to a stroller
Mulching lawnmower (Yes, it demonstrates both Principles 5 and 6, Merging and Universality.)
Team leader acts as recorder and timekeeper.
CCD (Charge coupled device) with micro-lenses formed on the surface
 

## Fractals or “Nested doll”

Place one object inside another; place each object, in turn, inside the other.
Measuring cups or spoons
Russian dolls
Portable audio system (microphone fits inside transmitter, which fits inside amplifier case)
Make one part pass through a cavity in the other.
Extending radio antenna
Extending pointer
Zoom lens
Seat belt retraction mechanism
Retractable aircraft landing gear stow inside the fuselage (also demonstrates Principle 15, Dynamism).
 

## Anti-weight

To compensate for the weight of an object, merge it with other objects that provide lift.
Inject foaming agent into a bundle of logs, to make it float better.
Use helium balloon to support advertising signs.
To compensate for the weight of an object, make it interact with the environment (e.g. use aerodynamic, hydrodynamic, buoyancy and other forces).
Aircraft wing shape reduces air density above the wing, increases density below wing, to create lift. (This also demonstrates Principle 4, Asymmetry.)
Vortex strips improve lift of aircraft wings.
Hydrofoils lift ship out of the water to reduce drag.
 

## Preliminary anti-action

If it will be necessary to do an action with both harmful and useful effects, this action should be replaced with anti-actions to control harmful effects.
Buffer a solution to prevent harm from extremes of pH.
Create beforehand stresses in an object that will oppose known undesirable working stresses later on.
Pre-stress rebar before pouring concrete.
Masking anything before harmful exposure: Use a lead apron on parts of the body not being exposed to X-rays. Use masking tape to protect the part of an object not being painted
 

## 10) Preliminary action

Perform, before it is needed, the required change of an object (either fully or partially).
Pre-pasted wall paper
Sterilize all instruments needed for a surgical procedure on a sealed tray.
Pre-arrange objects such that they can come into action from the most convenient place and without losing time for their delivery.
Kanban arrangements in a Just-In-Time factory
Flexible manufacturing cell
 

## 11) Beforehand cushioning

Prepare emergency means beforehand to compensate for the relatively low reliability of an object.
Magnetic strip on photographic film that directs the developer to compensate for poor exposure
Back-up parachute
Alternate air system for aircraft instruments
 

## 12) Equipotentiality

In a potential field, limit position changes (e.g. change operating conditions to eliminate the need to raise or lower objects in a gravity field).
Spring loaded parts delivery system in a factory
Locks in a channel between 2 bodies of water (Panama Canal)
“Skillets” in an automobile plant that bring all tools to the right position (also demonstrates Principle 10, Preliminary Action)
 

## 13) ‘The other way round’, chirality, [enantiomers](https://en.wikipedia.org/wiki/Enantiomer)

Invert the action(s) used to solve the problem (e.g. instead of cooling an object, heat it).
To loosen stuck parts, cool the inner part instead of heating the outer part.
Bring the mountain to Mohammed, instead of bringing Mohammed to the mountain.
Make movable parts (or the external environment) fixed, and fixed parts movable).
Rotate the part instead of the tool.
Moving sidewalk with standing people
Treadmill (for walking or running in place)
Turn the object (or process) ‘upside down’.
Turn an assembly upside down to insert fasteners (especially screws).
Empty grain from containers (ship or railroad) by inverting them.
 

## 14) Spheroidality – Curvature

Instead of using rectilinear parts, surfaces, or forms, use curvilinear ones; move from flat surfaces to spherical ones; from parts shaped as a cube (parallelepiped) to ball-shaped structures.
Use arches and domes for strength in architecture.
Use rollers, balls, spirals, domes.
Spiral gear (Nautilus) produces continuous resistance for weight lifting.
Ball point and roller point pens for smooth ink distribution
Go from linear to rotary motion, use centrifugal forces.
Produce linear motion of the cursor on the computer screen using a mouse or a trackball.
Replace wringing clothes to remove water with spinning clothes in a washing machine.
Use spherical casters instead of cylindrical wheels to move furniture.
 

Principle 15. Dynamics

Allow (or design) the characteristics of an object, external environment, or process to change to be optimal or to find an optimal operating condition.
Adjustable steering wheel (or seat, or back support, or mirror position…)
Divide an object into parts capable of movement relative to each other.
The “butterfly” computer keyboard, (also demonstrates Principle 7, “Nested doll”.)
If an object (or process) is rigid or inflexible, make it movable or adaptive.
The flexible boroscope for examining engines
The flexible sigmoidoscope, for medical examination
 

Principle 16. Partial or excessive actions

If 100 percent of an object is hard to achieve using a given solution method then, by using ‘slightly less’ or ‘slightly more’ of the same method, the problem may be considerably easier to solve.
Over spray when painting, then remove excess. (Or, use a stencil–this is an application of Principle 3, Local Quality and Principle 9, Preliminary anti-action).
Fill, then “top off” when filling the gas tank of your car.
 

Principle 17. Another dimension, 

To move an object in two- or three-dimensional space.
Infrared computer mouse moves in space, instead of on a surface, for presentations.
Five-axis cutting tool can be positioned where needed.
Use a multi-story arrangement of objects instead of a single-story arrangement.
Cassette with 6 CD’s to increase music time and variety
Electronic chips on both sides of a printed circuit board
Employees “disappear” from the customers in a theme park, descend into a tunnel, and walk to their next assignment, where they return to the surface and magically reappear.
Tilt or re-orient the object, lay it on its side.
Dump truck
Use ‘another side’ of a given area.
Stack microelectronic hybrid circuits to improve density.
 

## 18) Vibration or oscillation

Cause an object to oscillate or vibrate.
Electric carving knife with vibrating blades
Increase its frequency (even up to the ultrasonic).
Distribute powder with vibration.
Use an object’s resonant frequency.
Destroy gall stones or kidney stones using ultrasonic resonance.
Use piezoelectric vibrators instead of mechanical ones.
Quartz crystal oscillations drive high accuracy clocks.
Use combined ultrasonic and electromagnetic field oscillations.
Mixing alloys in an induction furnace
 

## 19) Periodic or pulsed action

Instead of continuous action, use periodic or pulsating actions.
Hitting something repeatedly with a hammer
Replace a continuous siren with a pulsed sound.
If an action is already periodic, change the periodic magnitude or frequency.
Use Frequency Modulation to convey information, instead of Morse code.
Replace a continuous siren with sound that changes amplitude and frequency.
Use pauses between impulses to perform a different action.
In cardio-pulmonary respiration (CPR) breathe after every 5 chest compressions.
 

## 20) Continuity of useful action

Carry on work continuously; make all parts of an object work at full load, all the time.
Structure overwinters, then leafs out in spring to rapidly achieve solar canopy
Flywheel (or hydraulic system) stores energy when a vehicle stops, so the motor can keep running at optimum power.
Run the bottleneck operations in a factory continuously, to reach the optimum pace. (From theory of constraints, or takt time operations)
Eliminate all idle or intermittent actions or work.
Print during the return of a printer carriage–dot matrix printer, daisy wheel printers, inkjet printers.
 

## 21) Skipping, microenvironments, cellular organelles

Conduct a process , or certain stages (e.g. destructible, harmful or hazardous operations) at high speed.
Use a high speed dentist’s drill to avoid heating tissue.
Cut plastic faster than heat can propagate in the material, to avoid deforming the shape.
 

## 22) “Blessing in disguise” or “Turn Lemons into Lemonade”

Use harmful factors (particularly, harmful effects of the environment or surroundings) to achieve a positive effect.
Poison, venom, biochemicals to subdue an enemy OR as drug to trigger a response.
Use waste heat to generate electric power.
Recycle waste (scrap) material from one process as raw materials for another.
Eliminate the primary harmful action by adding it to another harmful action to resolve the problem.
Add a buffering material to a corrosive solution.
Use a helium-oxygen mix for diving, to eliminate both nitrogen narcosis and oxygen poisoning from air and other nitrox mixes.
Amplify a harmful factor to such a degree that it is no longer harmful.
Use a backfire to eliminate the fuel from a forest fire.
 

## 23) Feedback, signalling, [amino acids](https://en.wikipedia.org/wiki/Amino_acid), [adenosine triphosphate](https://en.wikipedia.org/wiki/Adenosine_triphosphate)

Intercellular energy transfer, hormonal signalling, neurotransmission, passing genetic traits
Introduce feedback (referring back, cross-checking) to improve a process or action.
Automatic volume control in audio circuits
Signal from gyrocompass is used to control simple aircraft autopilots.
Statistical Process Control (SPC) — Measurements are used to decide when to modify a process. (Not all feedback systems are automated!)
Budgets –Measurements are used to decide when to modify a process.
If feedback is already used, change its magnitude or influence.
Change sensitivity of an autopilot when within 5 miles of an airport.
Change sensitivity of a thermostat when cooling vs. heating, since it uses energy less efficiently when cooling.
Change a management measure from budget variance to customer satisfaction.
 

## 24) Intermediary, secession species, ecoystem handoffs

Use an intermediary carrier article or intermediary process.
Carpenter’s nailset, used between the hammer and the nail
Merge one object temporarily with another (which can be easily removed).
Pot holder to carry hot dishes to the table
 

Principle 25. Self-service

Make an object serve itself by performing auxiliary helpful functions
A soda fountain pump that runs on the pressure of the carbon dioxide that is used to “fizz” the drinks. This assures that drinks will not be flat, and eliminates the need for sensors.
Halogen lamps regenerate the filament during use–evaporated material is redeposited.
To weld steel to aluminum, create an interface from alternating thin strips of the 2 materials. Cold weld the surface into a single unit with steel on one face and copper on the other, then use normal welding techniques to attach the steel object to the interface, and the interface to the aluminum. (This concept also has elements of Principle 24, Intermediary, and Principle 4, Asymmetry.)
Use waste resources, energy, or substances.
Use heat from a process to generate electricity: “Co-generation”.
Use animal waste as fertilizer.
Use food and lawn waste to create compost.
 

## 26) Copying, DNA

Instead of an unavailable, expensive, fragile object, use simpler and inexpensive copies.
Virtual reality via computer instead of an expensive vacation
Listen to an audio tape instead of attending a seminar.
Replace an object, or process with optical copies.
Do surveying from space photographs instead of on the ground.
Measure an object by measuring the photograph.
Make sonograms to evaluate the health of a fetus, instead of risking damage by direct testing.
If visible optical copies are already used, move to infrared or ultraviolet copies.
Make images in infrared to detect heat sources, such as diseases in crops, or intruders in a security system.
 

Principle 27. Cheap short-living objects

Replace an inexpensive object with a multiple of inexpensive objects, comprising certain qualities (such as service life, for instance).
Use disposable paper objects to avoid the cost of cleaning and storing durable objects. Plastic cups in motels, disposable diapers, many kinds of medical supplies.
 

Principle 28. Mechanics substitution

Replace a mechanical means with a sensory (optical, acoustic, taste or smell) means.
Replace a physical fence to confine a dog or cat with an acoustic “fence” (signal audible to the animal).
Use a bad smelling compound in natural gas to alert users to leakage, instead of a mechanical or electrical sensor.
Use electric, magnetic and electromagnetic fields to interact with the object.
To mix 2 powders, electrostatically charge one positive and the other negative. Either use fields to direct them, or mix them mechanically and let their acquired fields cause the grains of powder to pair up.
Change from static to movable fields, from unstructured fields to those having structure.
Early communications used omnidirectional broadcasting. We now use antennas with very detailed structure of the pattern of radiation.
Use fields in conjunction with field-activated (e.g. ferromagnetic) particles.
Heat a substance containing ferromagnetic material by using varying magnetic field. When the temperature exceeds the Curie point, the material becomes paramagnetic, and no longer absorbs heat.
 

## 29 Viscoeleastics, hydraulics, pneumatics

Use gelled, liquid, gaseous, plasma instead of just solid parts (e.g. inflatable, filled with liquids, air cushion, hydrostatic, hydro-reactive).
Comfortable shoe sole inserts filled with gel
Store energy from decelerating a vehicle in a hydraulic system, then use the stored energy to accelerate later.
 

Principle 30. Flexible shells and thin films

Use flexible shells and thin films instead of three dimensional structures
Use inflatable (thin film) structures as winter covers on tennis courts.
Isolate the object from the external environment using flexible shells and thin films.
Float a film of bipolar material (one end hydrophilic, one end hydrophobic) on a reservoir to limit evaporation.
 

Principle 31. Porous materials

Make an object porous or add porous elements (inserts, coatings, etc.).
Drill holes in a structure to reduce the weight.
If an object is already porous, use the pores to introduce a useful substance or function.
Use a porous metal mesh to wick excess solder away from a joint.
Store hydrogen in the pores of a palladium sponge. (Fuel “tank” for the hydrogen car–much safer than storing hydrogen gas)
 

Principle 32. Color changes

Change the color of an object or its external environment.
Use safe lights in a photographic darkroom.
Change the transparency of an object or its external environment.
Use photolithography to change transparent material to a solid mask for semiconductor processing. Similarly, change mask material from transparent to opaque for silk screen processing.
 

Principle 33. Homogeneity

Make objects interacting with a given object of the same material (or material with identical properties).
Make the container out of the same material as the contents, to reduce chemical reactions.
Make a diamond cutting tool out of diamonds.
 

Principle 34. Discarding and recovering

Make portions of an object that have fulfilled their functions go away (discard by dissolving, evaporating, etc.) or modify these directly during operation.
Use a dissolving capsule for medicine.
Sprinkle water on cornstarch-based packaging and watch it reduce its volume by more than 1000X!
Ice structures: use water ice or carbon dioxide (dry ice) to make a template for a rammed earth structure, such as a temporary dam. Fill with earth, then, let the ice melt or sublime to leave the final structure.
Conversely, restore consumable parts of an object directly in operation.
Self-sharpening lawn mower blades
Automobile engines that give themselves a “tune up” while running (the ones that say “100,000 miles between tune ups”)
 

Principle 35. Parameter changes

A. Change an object’s physical state (e.g. to a gas, liquid, or solid.
Freeze the liquid centers of filled candies, then dip in melted chocolate, instead of handling the messy, gooey, hot liquid.
Transport oxygen or nitrogen or petroleum gas as a liquid, instead of a gas, to reduce volume.
Change the concentration or consistency.
Liquid hand soap is concentrated and more viscous than bar soap at the point of use, making it easier to dispense in the correct amount and more sanitary when shared by several people.
Change the degree of flexibility.
Use adjustable dampers to reduce the noise of parts falling into a container by restricting the motion of the walls of the container.
Vulcanize rubber to change its flexibility and durability.
Change the temperature.
Raise the temperature above the Curie point to change a ferromagnetic substance to a paramagnetic substance.
Raise the temperature of food to cook it. (Changes taste, aroma, texture, chemical properties, etc.)
Lower the temperature of medical specimens to preserve them for later analysis.
 

Principle 36. Phase transitions

Use phenomena occurring during phase transitions (e.g. volume changes, loss or absorption of heat, etc.).
Water expands when frozen, unlike most other liquids. Hannibal is reputed to have used this when marching on Rome a few thousand years ago. Large rocks blocked passages in the Alps. He poured water on them at night. The overnight cold froze the water, and the expansion split the rocks into small pieces which could be pushed aside.
Heat pumps use the heat of vaporization and heat of condensation of a closed thermodynamic cycle to do useful work.
 

Principle 37. Thermal expansion

Use thermal expansion (or contraction) of materials.
Fit a tight joint together by cooling the inner part to contract, heating the outer part to expand, putting the joint together, and returning to equilibrium.
If thermal expansion is being used, use multiple materials with different coefficients of thermal expansion.
The basic leaf spring thermostat: (2 metals with different coefficients of expansion are linked so that it bends one way when warmer than nominal and the opposite way when cooler.)
 

Principle 38. Strong oxidants

Replace common air with oxygen-enriched air.
Scuba diving with Nitrox or other non-air mixtures for extended endurance
Replace enriched air with pure oxygen.
Cut at a higher temperature using an oxy-acetylene torch.
Treat wounds in a high pressure oxygen environment to kill anaerobic bacteria and aid healing.
Expose air or oxygen to ionizing radiation.
Use ionized oxygen.
Ionize air to trap pollutants in an air cleaner.
Replace ozonized (or ionized) oxygen with ozone.
Speed up chemical reactions by ionizing the gas before use.
 

Principle 39. Inert atmosphere

Replace a normal environment with an inert one.
Prevent degradation of a hot metal filament by using an argon atmosphere.
Add neutral parts, or inert additives to an object.
Increase the volume of powdered detergent by adding inert ingredients. This makes it easier to measure with conventional tools.
 

Principle 40. Composite materials

Change from uniform to composite (multiple) materials.
Composite epoxy resin/carbon fiber golf club shafts are lighter, stronger, and more flexible than metal. Same for airplane parts.
Fiberglass surfboards are lighter and more controllable and easier to form into a variety of shapes than wooden ones.