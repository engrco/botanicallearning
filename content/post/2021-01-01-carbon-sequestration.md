---
title: Carbon Sequestration
subtitle: Carbon nanotubes, pyrolysis, biochar, soil building, perennial polyculture 
date: 2021-01-01
tags: ["carbon", "sequestration"]
---

Carbon is VERY old news, carbon fuel economies, carbon sequestration and even carbon sequestration crackerjack prizes are *boring* ... how about if we go for a walk, look at Nature and just THINK ... maybe get out the periodic table and review why carbon has the role it has ... or better yet, how about if we putter in our greenhouse or garden and THINKER and cogitate deeply, BEYOND just the [wikipedia entry on photosynthesis](https://en.wikipedia.org/wiki/Photosynthesis)  ... the point is fantasy and science fiction and telling stories are absolutely necessary for dreaming and dreams are essential for Life -- but if we are going to make something work, we should understand what works from having our hands dirty -- and practical awareness of Nature and gardening, should understand why the best way to sequester carbon is to put it to better use cycle of Life.

Let's think about a global canopy of computing photosynthetic biofilm that uses the biophysics of photosynthesis to sequester many gigatons of carbon OPTIMALLY and INTELLIGENTLY ... NOT *stupidly* or *carelessly* because sequestration is the rage, but, correctly, as it can best be done, O P T I M A L L Y ... in the elegant fashion of Nature into biochemicals, bioenergy and the biostructures of the global canopy.

But maybe we should first, recap the carbon newstory from the beginning for those who were not paying attention in class ... the first bajillion times we covered this.