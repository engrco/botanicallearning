## Intelligent Plant Enviroments

Not just plant growth modeling and environmental control systems, but what about ... plant environments that sense, compute and THINK INTELLIGENTLY on the Edge. 

### Can we organically fabicate a sensing, thinking, computing intelligent botantical enviroment?

*Let's think about terraforming .... and Except for our work in carbon nanotechnology, that probably sounds a bit too much like pure science fiction, right?*  

Yes, it is very developmental ... our seed stage cloudlet toyhacks are definitely nothing but toys for thinkering with, maybe helping us to gain familiarity with development of languages, operating systems, closer-to-metal instruction sets ... like [quantum.country](https://quantum.country/qcvc) or [learning with quantum computing matrix algebra with Juypter notebooks](https://qiskit.org/learn) per [the development strategy of Qiskit](https://qiskit.org/documentation/development_strategy.html), this is really *just a* learning exercise at this point.

Hopefully, the generally hazy outlines of the Botanical.Cloud or Botanical.Computer vision are clear enough ... the objective is carbon-based photosynthetic qbitiquous quantum computing and we are darned serious about that objective.

*Right now, we just joke about it or don't take ourselves too seriously ... realizing that we're early in the chicken-and-egg development process, long before the first feathered Neanderthals walked upright and opened their startup in a garage in Silicon Valley.*

 At this time, Botanical.Computer is still ONLY a developing collection of learning resources and Git repos ... maybe, that alone, will be more than enough for a couple years ... and maybe the point is to spark inprovisation in others OR to find out about what is already out there.  For example, maybe someone else has already put together kick-ass multi-dimensional immersive learning idea like the [inner circle for learning jazz standards](https://members.learnjazzstandards.com/ljs-inner-circle/) ... it's really up to YOU ... if you choose, you can shape the whole project if you have better ideas ... the first objective assignment of this learning assignment is to just develop the process to develop hardened deliverables like a specification or even a specification for a debugger.  
 
 It's a speculative dream that involves looking seriously in how we might go about accomplishing something that delivers something beyond current mechanized learning and actually achieves something akin to intuition.  At first, we work within the machine learning or AI paradigms -- say, for instance, by developing a test quantum biocircuit to augment a RISC-V system-on-chip IoT hub processor ... the point is to use forests and oceans of plants to INTUIT, perhaps slowly, subconciously, but to step beyond mechanical calculations of reason, to actually ***intuit*** ... it's the start of a group of people who share a Nature-inspired intuition of how we would use bioengineered tissues and photosynthetic bioreactors to fabricate the neurons of deep learning consciousness.